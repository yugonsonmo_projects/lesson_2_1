
import asyncio
import requests
import re

class WeathData:
    def __init__(self, temp, humidity, pressure, wind):
        self.temp = temp
        self.humidity = humidity
        self.pressure = pressure
        self.wind = wind

openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'

with open('cities.txt', 'r', encoding='utf-8') as file:
    data = file.read()
    cities = re.findall(r'\d+\)(?:\s)?(.+?)(?:\s)?-', data)
    print(cities)

async def weather_param(city):
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={openweatherAPI}&units=metric"
    response = await loop.run_in_executor(None, requests.get, url)
    weather_data = response.json()
    temp = weather_data['main']['temp']
    pressure = weather_data['main']['pressure']
    humidity = weather_data['main']['humidity']
    wind = weather_data['wind']["speed"]
    print(f'Погода в {city}: \n'
          f'Температура {temp} C°,\n'
          f'Давление {pressure} мм.р.т., \n'
          f'Влажность {humidity}%, \n'
          f'Ветер {wind}м/с\n'
          f'')
    return WeathData(temp=temp, humidity=humidity, pressure=pressure, wind=wind)

async def main():


    tasks = [weather_param(city) for city in cities]
    await asyncio.gather(*tasks)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())

# cities = ["Moscow","Saint Petersburg", "Novosibirsk",
#          "Yekaterinburg", "Kazan'", "Nizhny Novgorod",
#          "Krasnoyarsk", "Chelyabinsk", "Samara", "Ufa"]




